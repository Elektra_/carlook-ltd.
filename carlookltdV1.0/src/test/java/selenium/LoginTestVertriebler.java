package selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class LoginTestVertriebler {

    private WebDriver driver = null;

    @Before
    public void setUpClass(){

        //set the property for webdriver.chrome.diver to be the location to your local download of chromedriver
        System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Google/Chrome/Application/chromedriver_85/chromedriver.exe");

        //Create new Instance of ChromeDiver
        driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 30);
    }

    @Test
    public void startWebDriver() throws InterruptedException {

        //Maximize the page
        driver.manage().window().maximize();

        //visit website
        driver.get("http://localhost:8080/CarlookLTD/#!Login");

        //Find the text input element by its name & enter something to search for
        driver.findElement(By.xpath("//*[@id=\"gwt-uid-3\"]")).sendKeys("LeaMustermann");

        //Find the text input element by its name & enter something to search for
        driver.findElement(By.xpath("//*[@id=\"gwt-uid-5\"]")).sendKeys("123456");

        //Click on Login-Button
        driver.findElement(By.xpath("//*[@id=\"ROOT-2521314\"]/div/div[2]/div/div/div/div[2]/div/div[7]/div")).click();

        Thread.sleep(2000);

        //Check if equals
        assertEquals("http://localhost:8080/CarlookLTD/#!Autos%20verwalten", driver.getCurrentUrl());
    }

    @After
    public void tearDownClass() {
        //Close the browser
        driver.quit();
    }
}

