package org.carlookltd.gui.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.carlookltd.gui.components.TopPanel;
import org.carlookltd.gui.container.ListReservierungContainer;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.services.util.Views;
import org.carlookltd.ui.MyUI;


/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class ListReservierungView extends VerticalLayout implements View {

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event){
        UserDTO user = ((MyUI) UI.getCurrent()).getUser();

        if (user == null) {
            UI.getCurrent().getNavigator().navigateTo(Views.LOGIN);
        } else {
            this.setUp();
        }
    }

    private void setUp(){
        VerticalLayout layout = new VerticalLayout();
        this.addComponent(new TopPanel());
        //this.addComponent(new Label("<hr />", ContentMode.HTML));
        MarginInfo marginInfo = new MarginInfo(false,true,true,true);
        layout.setMargin(marginInfo);
        this.setWidth("100%");
        this.setHeightUndefined();
        ListReservierungContainer listReservierungContainer = new ListReservierungContainer(layout);
        layout.addComponent(listReservierungContainer);
        this.addComponent(layout);
    }
}
