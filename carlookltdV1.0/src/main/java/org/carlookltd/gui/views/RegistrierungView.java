package org.carlookltd.gui.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import org.carlookltd.gui.container.RegistrierungContainer;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class RegistrierungView extends VerticalLayout implements View {

    public void setUp()   {

        this.setSizeFull();

        VerticalLayout layout = new VerticalLayout();

        Panel panel = new Panel("Bitte geben Sie Ihre Daten ein:");
        panel.addStyleName("register");

        this.addComponent(panel);
        this.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        panel.setContent(layout);

        panel.setSizeUndefined();

        new RegistrierungContainer(layout);

    }

    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        this.setUp();
    }

}
