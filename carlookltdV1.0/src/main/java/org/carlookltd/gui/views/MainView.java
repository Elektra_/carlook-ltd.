package org.carlookltd.gui.views;

import com.vaadin.data.HasValue;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.ItemClickListener;
import org.carlookltd.gui.components.TopPanel;
import org.carlookltd.services.util.TextFieldUtil;
import org.carlookltd.gui.windows.ReservierungWindow;
import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.process.control.SucheControl;
import org.carlookltd.services.util.Views;
import org.carlookltd.ui.MyUI;
import java.util.List;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class MainView extends VerticalLayout implements View {

    private int anzahlSuche = 0;
    private String sucheingabe;
    private boolean gridCreated = false;
    final Grid<AutoDTO> data = new Grid<>();
    final Button reserveButton = new Button(TextFieldUtil.RESERVIEREN);
    private AutoDTO autoSelektiert = null;

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        UserDTO user = ((MyUI) UI.getCurrent()).getUser();

        //Damit ohne Login nicht direkt auf Startseite zugegriffen werden kann
        if (user == null) {
            UI.getCurrent().getNavigator().navigateTo(Views.LOGIN);
        } else {
            this.setUp();
        }
    }

    public void setUp() {

        this.addComponent(new TopPanel());

        this.addComponent(new Label("<hr />", ContentMode.HTML));

        setMargin(true);

        this.setWidth("100%");
        this.setHeightUndefined();

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button button = new Button(TextFieldUtil.SUCHEN, FontAwesome.SEARCH);
        final TextField suchFeld = new TextField();

        UserDTO user = ((MyUI) UI.getCurrent()).getUser();

        Label labelText = new Label("Geben Sie einen Suchbegriff ein:");
        horizontalLayout.addComponent(labelText);
        horizontalLayout.setComponentAlignment(labelText, Alignment.MIDDLE_CENTER);
        horizontalLayout.addComponent(suchFeld);
        horizontalLayout.addComponent(new Label("&nbsp", ContentMode.HTML));
        horizontalLayout.addComponent(button);

        this.addComponent(horizontalLayout);
        setComponentAlignment(horizontalLayout, Alignment.MIDDLE_CENTER);

        data.setId("id");

        data.setSizeFull();
        data.setSelectionMode(Grid.SelectionMode.SINGLE);

        reserveButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {

                if (MainView.this.autoSelektiert == null) {
                    Notification.show("Bitte wählen Sie ein Auto aus.",Notification.Type.WARNING_MESSAGE);
                }
                else {
                    //oeffne neues Window { autoSelektiert } zur Reservierung...
                    System.out.println("Auto selektiert " + MainView.this.autoSelektiert.getBeschreibung());

                    ReservierungWindow window = new ReservierungWindow(MainView.this.autoSelektiert);
                    UI.getCurrent().addWindow(window);
                }
            }
        });

        data.addItemClickListener(new ItemClickListener<AutoDTO>() {
            @Override
            public void itemClick(Grid.ItemClick<AutoDTO> itemClick) {
                System.out.println("Zeile selektiert: " + itemClick.getItem().getAutoid());

                //Abspeichern des aktuellen (angeklickten) Hotels
                autoSelektiert = itemClick.getItem();

            }
        });

        // Suche über den Button
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                sucheingabe = suchFeld.getValue();

                if (sucheingabe.equals("")) {
                    Notification.show(null, "Bitte geben Sie einen Suchbegriff ein:", Notification.Type.WARNING_MESSAGE);
                } else {

                    //Zeigt die Anzahl der Sucheingaben an
                    MainView.this.anzahlSuche++;
                    data.setCaption("Ergebnisse für " + sucheingabe + " (Anzahl der Suche: " + MainView.this.anzahlSuche + ")"
                            +  (((MyUI) UI.getCurrent()).getUser()).getBenutzername());

                    //Tabelle befüllen
                    List<AutoDTO> liste = SucheControl.getInstance().getAutoByMarkeOrBezeichnungOrBaujahr(sucheingabe);

                    //Liste in Container einsetzen
                    data.setItems(liste);

                    if(!gridCreated){
                        addGridComponentAndButton();
                    }
                    else{
                        data.getDataProvider().refreshAll();
                    }

                    //data.setHeightByRows((double) liste.size());
                }
            }
        });

        //On-The-Fly-Suche
        suchFeld.addValueChangeListener(new HasValue.ValueChangeListener<String>() {

            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> valueChangeEvent) {
                if(suchFeld.getValue().equals("")){
                    Notification.show("Bitte geben Sie einen Suchbegriff ein.");
                }
                else {
                    sucheingabe = suchFeld.getValue();
                    if(!gridCreated){
                        addGridComponentAndButton();
                    }
                    List<AutoDTO> liste = SucheControl.getInstance().getAutoByMarkeOrBezeichnungOrBaujahr(sucheingabe);
                    data.setItems(liste);
                    data.getDataProvider().refreshAll();
                    data.setCaption(liste.size() + " Ergebniss(e) für \"" + sucheingabe + "\"");
                }
            }
        });

    }

    private void addGridComponentAndButton(){
        data.addColumn(AutoDTO::getMarke).setCaption("Marke");
        data.addColumn(AutoDTO::getBeschreibung).setCaption("Beschreibung");
        data.addColumn(AutoDTO::getBaujahr).setCaption("Baujahr");
        addComponent(data);
        data.setHeightByRows(5);
        addComponent(reserveButton);
        setComponentAlignment(reserveButton,Alignment.MIDDLE_CENTER);
        gridCreated = true;
    }

}
