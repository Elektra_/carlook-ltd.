package org.carlookltd.gui.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import org.carlookltd.gui.container.LoginContainer;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.services.util.Views;
import org.carlookltd.ui.MyUI;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class LoginView extends VerticalLayout implements View {

    public void setUp() {

        this.setSizeFull();

        VerticalLayout layout = new VerticalLayout();

        Panel panel = new Panel("Login");
        panel.addStyleName("login");

        this.addComponent(panel);
        this.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        panel.setContent(layout);

        panel.setSizeUndefined();

        new LoginContainer(layout);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        //User user = (User) VaadinSession.getCurrent().getAttribute(Roles.CURRENT_USER);
        UserDTO user = ((MyUI) UI.getCurrent()).getUser();

        if(user != null) {
            UI.getCurrent().getNavigator().navigateTo(Views.MAIN);
        } else {
            this.setUp();
        }
    }

}
