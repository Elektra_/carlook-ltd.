package org.carlookltd.gui.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import org.carlookltd.gui.components.TopPanel;
import org.carlookltd.gui.container.AutoVerwaltungContainer;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.services.util.Views;
import org.carlookltd.ui.MyUI;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class AutosVerwaltenView extends VerticalLayout implements View {

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event){
        UserDTO user = ((MyUI) UI.getCurrent()).getUser();

        if (user == null) {
            UI.getCurrent().getNavigator().navigateTo(Views.LOGIN);
        } else {
            this.setUp();
        }
    }

    private void setUp(){
        VerticalLayout verticalLayout = new VerticalLayout();
        this.addComponent(new TopPanel());
        this.addComponent(new Label("<hr />", ContentMode.HTML));
        setMargin(true);
        this.setWidth("100%");
        this.setHeightUndefined();

        AutoVerwaltungContainer autoVerwaltungContainer = new AutoVerwaltungContainer(verticalLayout);
        verticalLayout.addComponent(autoVerwaltungContainer);
        this.addComponent(verticalLayout);
    }
}
