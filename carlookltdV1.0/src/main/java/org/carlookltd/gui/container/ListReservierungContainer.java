package org.carlookltd.gui.container;

import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import org.carlookltd.model.dao.UserDAO;
import org.carlookltd.model.dto.ReservierungDTO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.process.control.ReservierungControl;
import org.carlookltd.process.control.exceptions.DatabaseException;

import java.util.List;

public class ListReservierungContainer extends VerticalLayout {
    List<ReservierungDTO> liste;

    public ListReservierungContainer(VerticalLayout verticalLayout){
        this.setMargin(false);
        final Label h1 = new Label();
        h1.setCaption("Ihre Reservierungen");
        Grid<ReservierungDTO> grid = new Grid<>();
        grid.setId("id");

        liste = ReservierungControl.getInstance().getAllReservierungForUser();

        grid.setItems(liste);
        grid.addColumn(ReservierungDTO::getReservierungsnummer).setCaption("Buchungsnummer");
        grid.addColumn(ReservierungsDTO -> ReservierungsDTO.getAutoDTO().getMarke()).setCaption("Marke");
        grid.addColumn(ReservierungsDTO -> ReservierungsDTO.getAutoDTO().getBeschreibung()).setCaption("Beschreibung");
        grid.addColumn(ReservierungsDTO -> ReservierungsDTO.getAutoDTO().getKennzeichen()).setCaption("Kennzeichen");
        grid.addColumn(ReservierungDTO::getBuchungsDatum).setCaption("Buchungsdatum");
        grid.addColumn(ReservierungDTO::getAusleihDatum).setCaption("Ausleihdatum");
        grid.addColumn(ReservierungDTO::getRueckgabeDatum).setCaption("Rückgabedatum");
        grid.addColumn(ReservierungDTO::getIban).setCaption("IBAN");
        verticalLayout.addComponent(h1);
        verticalLayout.addComponent(grid);
        grid.setWidth("100%");
    }
}
