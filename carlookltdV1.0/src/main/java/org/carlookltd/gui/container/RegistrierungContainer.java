package org.carlookltd.gui.container;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.carlookltd.services.util.TextFieldUtil;
import org.carlookltd.process.control.RegistrierungControl;
import org.carlookltd.process.control.exceptions.DatabaseException;
import org.carlookltd.process.control.exceptions.RegistrierungException;
import org.carlookltd.services.util.Rolle;
import org.carlookltd.services.util.Views;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class RegistrierungContainer extends VerticalLayout {

    public RegistrierungContainer(VerticalLayout layout){

        final TextField userLogin = new TextField();
        userLogin.setCaption(TextFieldUtil.BENUTZERNAME + ":");

        final PasswordField passwordField = new PasswordField();
        passwordField.setCaption(TextFieldUtil.PASSWORT + ":");

        final PasswordField passwordFieldrepeat = new PasswordField();
        passwordFieldrepeat.setCaption(TextFieldUtil.PASSWORT_CONFIRM + ":");

        final TextField emailField = new TextField();
        emailField.setCaption(TextFieldUtil.EMAIL + ":");

        final TextField emailFieldrepeat = new TextField();
        emailFieldrepeat.setCaption(TextFieldUtil.EMAIL_CONFIRM + ":");

        final ComboBox rolleBox = new ComboBox();
        rolleBox.setId(TextFieldUtil.COMBOBOX_ROLLE);
        rolleBox.setCaption("Ich bin:");
        //String endkunde = Rolle.ENDKUNDE;
        //String vertriebler = Rolle.VERTRIEBLER;
        //rolleBox.setItems(endkunde, vertriebler);

        Label endkunde = new Label(Rolle.ENDKUNDE);
        endkunde.setId(TextFieldUtil.ROLLE_ENDKUNDE);
        Label vertriebler = new Label(Rolle.VERTRIEBLER);
        vertriebler.setId(TextFieldUtil.ROLLE_VERTRIEBLER);
        rolleBox.setItems(endkunde,vertriebler);

        rolleBox.setItemCaptionGenerator(new ItemCaptionGenerator() {
            @Override
            public String apply(Object item) {
                return ((Label)item).getValue();
            }
        });

        layout.addComponent(userLogin);
        layout.setComponentAlignment(userLogin, Alignment.MIDDLE_CENTER);
        layout.addComponent(passwordField);
        layout.setComponentAlignment(passwordField, Alignment.MIDDLE_CENTER);
        layout.addComponent(passwordFieldrepeat);
        layout.setComponentAlignment(passwordFieldrepeat, Alignment.MIDDLE_CENTER);
        layout.addComponent(emailField);
        layout.setComponentAlignment(emailField, Alignment.MIDDLE_CENTER);
        layout.addComponent(emailFieldrepeat);
        layout.setComponentAlignment(emailFieldrepeat, Alignment.MIDDLE_CENTER);
        layout.addComponent(rolleBox);
        layout.setComponentAlignment(rolleBox, Alignment.MIDDLE_CENTER);

        Label label = new Label("&nbsp;", ContentMode.HTML);
        layout.addComponent(label);

        //Beide Button nebeneinander platzieren
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        //Registrieren-Button
        Button buttonRegistrieren = new Button(TextFieldUtil.REGISTRIEREN);
        buttonRegistrieren.addStyleName(ValoTheme.BUTTON_PRIMARY);
        horizontalLayout.addComponent(buttonRegistrieren);
        horizontalLayout.setComponentAlignment(buttonRegistrieren, Alignment.MIDDLE_CENTER);

        buttonRegistrieren.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    String login = userLogin.getValue();
                    String password = passwordField.getValue();
                    String passwordrepeat = passwordFieldrepeat.getValue();
                    String email = emailField.getValue();
                    String emailrepeat = emailFieldrepeat.getValue();
                    String rolle = ((Label) rolleBox.getValue()).getValue();

                    try {
                        RegistrierungControl.getInstance().checkRegistrierung(login, password, passwordrepeat, email, emailrepeat, rolle);
                    } catch (RegistrierungException | DatabaseException e) {
                        Notification.show(e.getMessage(), Notification.Type.ERROR_MESSAGE);
                    }
                } catch (NullPointerException e) {
                    Notification.show("Sie müssen auswählen, ob Sie Endkunde oder Vertriebler sind.", Notification.Type.ERROR_MESSAGE);
                }
            }
        });


        //Abbrechen-Button
        Button buttonAbbrechen = new Button(TextFieldUtil.ABBRECHEN);
        horizontalLayout.addComponent(buttonAbbrechen);
        horizontalLayout.setComponentAlignment(buttonAbbrechen, Alignment.MIDDLE_CENTER);

        buttonAbbrechen.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                UI.getCurrent().getNavigator().navigateTo(Views.LOGIN);
            }
        });

        layout.addComponent(horizontalLayout);
        layout.setComponentAlignment(horizontalLayout, Alignment.MIDDLE_CENTER);

    }


}
