package org.carlookltd.gui.container;

import com.vaadin.server.ExternalResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.carlookltd.services.util.TextFieldUtil;
import org.carlookltd.process.control.LoginControl;
import org.carlookltd.process.control.exceptions.DatabaseException;
import org.carlookltd.process.control.exceptions.NoSuchUserOrPasswordException;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class LoginContainer extends VerticalLayout {

    public LoginContainer(VerticalLayout layout) {

        final TextField userLogin = new TextField();
        userLogin.setCaption(TextFieldUtil.BENUTZERNAME + " oder " + TextFieldUtil.EMAIL + ":");

        final PasswordField passwordField = new PasswordField();
        passwordField.setCaption(TextFieldUtil.PASSWORT + ":");

        layout.addComponent(userLogin);
        layout.setComponentAlignment(userLogin, Alignment.MIDDLE_CENTER);
        layout.addComponent(passwordField);
        layout.setComponentAlignment(passwordField, Alignment.MIDDLE_CENTER);

        Label label = new Label("&nbsp;", ContentMode.HTML);
        layout.addComponent(label);

        Button buttonLogin = new Button(TextFieldUtil.LOGIN);
        buttonLogin.addStyleName(ValoTheme.BUTTON_PRIMARY);
        layout.addComponent(buttonLogin);
        layout.setComponentAlignment(buttonLogin, Alignment.MIDDLE_CENTER);

        buttonLogin.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                String login = userLogin.getValue();
                String password = passwordField.getValue();

                try {

                    LoginControl.checkAuthentication(login, password);

                } catch (NoSuchUserOrPasswordException noSuchUserOrPassword) {

                    Notification.show("Benutzer-Fehler", "Login oder Password falsch", Notification.Type.ERROR_MESSAGE);
                    userLogin.setValue("");     //Neues Login und Password eintragen
                    passwordField.setValue("");
                } catch (DatabaseException e) {
                    Notification.show("DB-Fehler", e.getReason() , Notification.Type.ERROR_MESSAGE);
                    userLogin.setValue("");     //Neues Login und Password eintragen
                    passwordField.setValue("");
                }
            }
        });

        HorizontalLayout horizontalLayout = new HorizontalLayout();

        Label nichtRegistriert = new Label("Noch nicht registriert?");
        Link link = new Link("Hier registrieren", new ExternalResource("http://localhost:8080/carlookltdV1.0#!Registrierung"));

        horizontalLayout.addComponent(nichtRegistriert);
        horizontalLayout.addComponent(link);
        horizontalLayout.setComponentAlignment(nichtRegistriert, Alignment.MIDDLE_CENTER);
        horizontalLayout.setComponentAlignment(link, Alignment.MIDDLE_CENTER);

        layout.addComponent(horizontalLayout);
        layout.setComponentAlignment(horizontalLayout, Alignment.MIDDLE_CENTER);
    }

}
