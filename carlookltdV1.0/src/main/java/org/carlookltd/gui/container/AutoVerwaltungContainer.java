package org.carlookltd.gui.container;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.ItemClickListener;
import org.carlookltd.gui.windows.AutoErstellenWindow;
import org.carlookltd.gui.windows.AutoLoeschenBestaetigenWindow;
import org.carlookltd.gui.windows.AutoUpdateWindow;
import org.carlookltd.model.dao.AutoDAO;
import org.carlookltd.model.dao.UserDAO;
import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.process.control.exceptions.DatabaseException;

import java.util.List;

/**
 *
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class AutoVerwaltungContainer extends VerticalLayout {
    List<AutoDTO> liste;
    AutoDTO autoSelektiert = null;
    UserDTO userDTO = null;

    public AutoVerwaltungContainer(VerticalLayout verticalLayout){

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        final Label h1 = new Label();
        h1.setCaption("Von Ihnen inserierte Autos");
        Grid<AutoDTO> grid = new Grid<>();
        grid.setId("id");

        try {
            userDTO = UserDAO.getInstance().getUserFromDB();
            liste = AutoDAO.getInstance().getAutosByVertrieblerId(userDTO.getBenutzerid());
        } catch (DatabaseException e) {
            Notification.show(e.getReason(),Notification.Type.ERROR_MESSAGE);
        }
        grid.setItems(liste);
        grid.addColumn(AutoDTO::getMarke).setCaption("Marke");
        grid.addColumn(AutoDTO::getBeschreibung).setCaption("Beschreibung");
        grid.addColumn(AutoDTO::getBaujahr).setCaption("Baujahr");
        grid.addColumn(AutoDTO::getKennzeichen).setCaption("Kennzeichen");
        grid.setHeightByRows(5);
        grid.setWidth("100%");

        final Button autoErstellen = new Button("Auto erstellen");
        final Button autoUpdate = new Button("Bearbeiten");
        final Button autoLoeschen = new Button("Löschen");

        verticalLayout.addComponent(h1);
        verticalLayout.addComponent(grid);


        horizontalLayout.addComponent(autoErstellen);
        horizontalLayout.addComponent(autoUpdate);
        horizontalLayout.addComponent(autoLoeschen);
        horizontalLayout.addComponent(new Label("&nbsp;", ContentMode.HTML));
        horizontalLayout.setComponentAlignment(autoErstellen, Alignment.MIDDLE_CENTER);
        horizontalLayout.setComponentAlignment(autoLoeschen,Alignment.MIDDLE_CENTER);
        horizontalLayout.setComponentAlignment(autoUpdate,Alignment.MIDDLE_CENTER);
        this.addComponent(horizontalLayout);
        this.setMargin(true);
        this.setComponentAlignment(horizontalLayout,Alignment.MIDDLE_LEFT);



        grid.addItemClickListener(new ItemClickListener<AutoDTO>() {
            @Override
            public void itemClick(Grid.ItemClick<AutoDTO> itemClick) {
                System.out.println("Selektiertes Auto: "+ itemClick.getItem().toString());
                autoSelektiert = itemClick.getItem();
            }
        });

        autoErstellen.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                AutoErstellenWindow autoErstellenWindow = new AutoErstellenWindow("Neues Auto");
                UI.getCurrent().addWindow(autoErstellenWindow);
                autoErstellenWindow.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent closeEvent) {
                        try {
                            liste = AutoDAO.getInstance().getAutosByVertrieblerId(userDTO.getBenutzerid());
                        } catch (DatabaseException e) {
                            e.printStackTrace();
                            Notification.show(e.getReason(),Notification.Type.ERROR_MESSAGE);
                        }
                        grid.setItems(liste);
                    }
                });
            }
        });

        autoLoeschen.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if(autoSelektiert == null){
                    Notification.show("Bitte wählen Sie zunächst ein Auto aus",Notification.Type.WARNING_MESSAGE);
                    return;
                }
                AutoLoeschenBestaetigenWindow autoLoeschenBestaetigenWindow = new AutoLoeschenBestaetigenWindow(autoSelektiert);
                UI.getCurrent().addWindow(autoLoeschenBestaetigenWindow);
                autoLoeschenBestaetigenWindow.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent closeEvent) {
                        try {
                            liste = AutoDAO.getInstance().getAutosByVertrieblerId(userDTO.getBenutzerid());
                        } catch (DatabaseException e) {
                            e.printStackTrace();
                            Notification.show(e.getReason(),Notification.Type.ERROR_MESSAGE);
                        }
                        grid.setItems(liste);
                    }
                });
            }
        });

        autoUpdate.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if(autoSelektiert == null){
                    Notification.show("Bitte wählen Sie ein Auto aus.", Notification.Type.WARNING_MESSAGE);
                    return;
                }
                AutoUpdateWindow autoUpdateWindow = new AutoUpdateWindow(autoSelektiert);
                UI.getCurrent().addWindow(autoUpdateWindow);

                autoUpdateWindow.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent closeEvent) {
                        try {
                            liste = AutoDAO.getInstance().getAutosByVertrieblerId(userDTO.getBenutzerid());
                        } catch (DatabaseException e) {
                            e.printStackTrace();
                            Notification.show(e.getReason(),Notification.Type.ERROR_MESSAGE);
                        }
                        grid.setItems(liste);
                    }
                });
            }
        });
    }
}
