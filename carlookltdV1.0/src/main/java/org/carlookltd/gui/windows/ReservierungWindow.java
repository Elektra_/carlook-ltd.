package org.carlookltd.gui.windows;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.model.dto.ReservierungDTO;
import org.carlookltd.process.control.ReservierungControl;
import org.carlookltd.ui.MyUI;

import java.time.ZoneId;
import java.util.Date;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class ReservierungWindow extends Window {

    public ReservierungWindow(final AutoDTO auto) {
        super("Reservierung");
        center();

        //Some basic content for the window
        VerticalLayout content = new VerticalLayout();
        content.addComponent(new Label("Reservierung für Auto: " + auto.getMarke() + ", " + auto.getBeschreibung()));
        content.setMargin(true);
        setContent(content);


        class CustomDateField extends CustomField<Date> {
            DateField dateField;

            public CustomDateField(String caption) {
                setCaption(caption);
                dateField = new DateField();
                dateField.setDateFormat("dd/MM/yy");
            }

            @Override
            protected Component initContent() {
                return dateField;
            }

            @Override
            protected void doSetValue(Date date) {
                dateField.setValue(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            }

            @Override
            public Date getValue() {
                return dateField.getValue() != null ? Date.from(dateField.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()) : null;
            }
        }


        final CustomDateField ausleihDateField = new CustomDateField("Ausleihen");
        content.addComponent(ausleihDateField);


        final CustomDateField rueckgabeDateField = new CustomDateField("Rückgabe");
        content.addComponent(rueckgabeDateField);

        final TextField iban = new TextField();
        iban.setCaption("IBAN");
        content.addComponent(iban);
        /*
        final EndkundeDTO endkunde = new EndkundeDTO();
        content.addComponent((Component) endkunde);
         */

        Label emptyLabel = new Label("&nbsp;", ContentMode.HTML);
        content.addComponent(emptyLabel);

        //Enable the close button
        setClosable(true);

        //Implementierung Button
        Button reserviereButton = new Button("Reserviere");
        reserviereButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Date currentDate = new Date();
                currentDate.setTime(System.currentTimeMillis());
                currentDate.setHours(0);
                currentDate.setMinutes(0);
                currentDate.setSeconds(0);
                Date yesterday = new Date();
                yesterday.setTime(currentDate.getTime());
                yesterday.setDate(yesterday.getDate()-1);

                if(ausleihDateField.isEmpty()){
                    Notification.show("Bitte geben Sie ein Ausleihdatum an.", Notification.Type.WARNING_MESSAGE);
                    return;
                }
                else if(rueckgabeDateField.isEmpty()){
                    Notification.show("Bitte geben Sie ein Rückgabedatum an.",Notification.Type.WARNING_MESSAGE);
                    return;
                }
                if(rueckgabeDateField.getValue().before(ausleihDateField.getValue())){
                    Notification.show("Rückgabedatum ist vor dem Ausleihdatum.", Notification.Type.WARNING_MESSAGE);
                    ausleihDateField.focus();
                    return;
                }
                else if(ausleihDateField.getValue().before(yesterday)){
                    Notification.show("Ausleihdatum liegt in der Vergangenheit.", Notification.Type.WARNING_MESSAGE);
                    ausleihDateField.focus();
                    return;
                }

                if(iban.getValue().isEmpty()){
                    Notification.show("Bitte geben Sie eine IBAN an.", Notification.Type.WARNING_MESSAGE);
                    iban.focus();
                    return;
                }


                ReservierungDTO request = new ReservierungDTO();
                request.setAusleihDatum( ausleihDateField.getValue());
                request.setRueckgabeDatum(rueckgabeDateField.getValue());
                request.setIban(iban.getValue());
                request.setBuchungsDatum(currentDate);
                request.setUser(((MyUI) MyUI.getCurrent()).getUser());
                System.out.println(request.getUser().getBenutzerid());
                request.setAutoDTO(auto);

                boolean check = ReservierungControl.getInstance().checkReserverierung(request);
                if(check){
                    close();
                    UI.getCurrent().addWindow(new ConfirmationWindow("Buchung erfolgreich: ID: " + request.getReservierungsnummer()));
                }
            }
        });

        content.addComponent(reserviereButton);
        content.setComponentAlignment(reserviereButton, Alignment.MIDDLE_CENTER);

    }

}
