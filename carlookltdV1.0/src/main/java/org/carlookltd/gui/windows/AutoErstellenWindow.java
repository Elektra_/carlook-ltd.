package org.carlookltd.gui.windows;


import com.vaadin.data.HasValue;
import com.vaadin.ui.*;
import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.process.control.AutoVerwaltenControl;
import org.carlookltd.ui.MyUI;

/**
 *
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class AutoErstellenWindow extends Window {

    public AutoErstellenWindow(String title){
        AutoDTO autoDTO = new AutoDTO();
        VerticalLayout layout = new VerticalLayout();
        center();
        Label h1 = new Label();
        h1.setCaption(title);
        layout.addComponent(h1);
        layout.setMargin(true);

        final TextField marke = new TextField();
        marke.setCaption("Marke");
        final TextField beschreibung = new TextField();
        beschreibung.setCaption("Beschreibung");
        final TextField kennzeichen = new TextField();
        kennzeichen.setCaption("Kennzeichen");
        final TextField baujahr = new TextField();
        baujahr.setCaption("Baujahr");

        final Button erstellen = new Button();
        erstellen.setCaption("Erstellen");
        final Button abbrechen = new Button();
        abbrechen.setCaption("Abbrechen");

        marke.setSizeFull();
        baujahr.setSizeFull();
        kennzeichen.setSizeFull();
        beschreibung.setSizeFull();

        layout.addComponent(marke);
        layout.addComponent(baujahr);
        layout.addComponent(kennzeichen);
        layout.addComponent(beschreibung);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(erstellen);
        horizontalLayout.addComponent(abbrechen);
        layout.addComponent(horizontalLayout);
        setContent(layout);

        baujahr.addValueChangeListener(new HasValue.ValueChangeListener<String>() {

           @Override
           public void valueChange(HasValue.ValueChangeEvent<String> valueChangeEvent) {
                try {
                    Integer.parseInt(baujahr.getValue());
                }
                catch (NumberFormatException e){
                    Notification.show("Bitte geben Sie eine Ganzzahl ein (bspw. 1995)", Notification.Type.WARNING_MESSAGE);
                    baujahr.clear();
                }
           }
       });

        erstellen.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if(marke.getValue().equals("")){
                    Notification.show("Bitte geben Sie einen Wert für die Marke ein.");
                    marke.focus();
                    return;
                }
                else if(beschreibung.getValue().equals("")){
                    Notification.show("Bitte geben Sie einen Wert für die Beschreibung ein.");
                    beschreibung.focus();
                    return;
                }
                else if(kennzeichen.getValue().equals("")){
                    Notification.show("Bitte geben Sie einen Wert für das Kennzeichen ein.");
                    kennzeichen.focus();
                    return;
                }
                else if(baujahr.getValue().isEmpty()){
                    Notification.show("Bitte geben Sie einen Wert für das Baujahr ein.");
                    baujahr.focus();
                    return;
                }

                autoDTO.setMarke(marke.getValue());
                autoDTO.setBeschreibung(beschreibung.getValue());
                autoDTO.setKennzeichen(kennzeichen.getValue());
                autoDTO.setBaujahr(Integer.parseInt(baujahr.getValue()));
                autoDTO.setVertrieblerId(((MyUI) UI.getCurrent()).getUser().getBenutzerid());
                AutoVerwaltenControl.autoErstellen(autoDTO);
                AutoErstellenConfirmationWindow autoErstellenConfirmationWindow = new AutoErstellenConfirmationWindow(autoDTO);
                UI.getCurrent().addWindow(autoErstellenConfirmationWindow);
                autoErstellenConfirmationWindow.addCloseListener(new CloseListener() {
                    @Override
                    public void windowClose(CloseEvent closeEvent) {
                        close();
                    }
                });
            }

        });

        abbrechen.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                close();
            }
        });
    }

}
