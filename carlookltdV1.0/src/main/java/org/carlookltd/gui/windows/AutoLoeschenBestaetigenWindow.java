package org.carlookltd.gui.windows;

import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.process.control.AutoVerwaltenControl;

/**
 *
 * @author Bugay Cetinel
 * Version 1.0
 *
 */

public class AutoLoeschenBestaetigenWindow extends Window {

    public AutoLoeschenBestaetigenWindow(AutoDTO autoDTO){
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        center();
        verticalLayout.addComponent(new Label("Löschen bestätigen"));
        verticalLayout.addComponent(new Label("Sind sie sich sicher, dass Sie folgendes Auto löschen möchten?"));
        verticalLayout.addComponent(new Label(autoDTO.toString()));

        final Button loeschen = new Button();
        loeschen.addStyleName(ValoTheme.BUTTON_DANGER);
        final Button abbrechen = new Button();
        loeschen.setCaption("Löschen");
        abbrechen.setCaption("Abbrechen");
        horizontalLayout.addComponent(loeschen);
        horizontalLayout.addComponent(abbrechen);
        verticalLayout.addComponent(horizontalLayout);
        setContent(verticalLayout);


        abbrechen.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                close();
            }
        });

        loeschen.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                AutoVerwaltenControl.autoLoeschen(autoDTO);
                close();
            }
        });
    }
}
