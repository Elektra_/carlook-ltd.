package org.carlookltd.gui.windows;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.carlookltd.model.dto.AutoDTO;

/**
 *
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class AutoErstellenConfirmationWindow extends Window {
    public AutoErstellenConfirmationWindow(AutoDTO autoDTO){
        VerticalLayout layout = new VerticalLayout();
        center();
        Label h1 = new Label();
        h1.setCaption("Bestätigung");
        Label confirmation = new Label("Auto erfolgreich erstellt:");
        Label auto = new Label(autoDTO.toString());
        final Button ok = new Button();
        ok.setCaption("Ok");
        layout.addComponent(h1);
        layout.addComponent(confirmation);
        layout.addComponent(auto);
        layout.addComponent(ok);
        setContent(layout);

        ok.addClickListener((Button.ClickListener) clickEvent -> close());
    }
}
