package org.carlookltd.gui.windows;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import org.carlookltd.model.dto.AutoDTO;

/**
 *
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class AutoUpdateConfirmationWindow extends Window {
    public AutoUpdateConfirmationWindow(AutoDTO newAutoDTO, AutoDTO oldAutoDTO){
        VerticalLayout verticalLayout = new VerticalLayout();
        setWidth("30%");
        center();
        verticalLayout.setMargin(true);
        Label h1 = new Label("Übernommene Änderungen");
        verticalLayout.addComponent(h1);

        Label markePointer = new Label();
        markePointer.setCaption(newAutoDTO.getMarke());
        Label beschreibungPointer = new Label();
        beschreibungPointer.setCaption(newAutoDTO.getBeschreibung());
        Label baujahrPointer = new Label();
        baujahrPointer.setCaption(String.valueOf(newAutoDTO.getBaujahr()));
        Label kennzeichenPointer = new Label();
        kennzeichenPointer.setCaption(newAutoDTO.getKennzeichen());

        boolean somethingChanged = false;

        if(!newAutoDTO.getMarke().equals(oldAutoDTO.getMarke())){
            markePointer.setIcon(FontAwesome.SAVE);
            somethingChanged = true;
        }
        if(!newAutoDTO.getBeschreibung().equals(oldAutoDTO.getBeschreibung())){
            beschreibungPointer.setIcon(FontAwesome.SAVE);
            somethingChanged = true;
        }
        if(newAutoDTO.getBaujahr() != oldAutoDTO.getBaujahr()){
            baujahrPointer.setIcon(FontAwesome.SAVE);
            somethingChanged = true;
        }
        if(!newAutoDTO.getKennzeichen().equals(oldAutoDTO.getKennzeichen())){
            kennzeichenPointer.setIcon(FontAwesome.SAVE);
            somethingChanged = true;
        }

        Button ok = new Button("ok");

        if(somethingChanged){
            verticalLayout.addComponent(new Label("Marke:"));
            verticalLayout.addComponent(markePointer);
            verticalLayout.addComponent(new Label("Baujahr:"));
            verticalLayout.addComponent(baujahrPointer);
            verticalLayout.addComponent(new Label("Beschreibung:"));
            verticalLayout.addComponent(beschreibungPointer);
            verticalLayout.addComponent(new Label("Kennzeichen:"));
            verticalLayout.addComponent(kennzeichenPointer);
        }
        else {
            verticalLayout.addComponent(new Label("Keine Änderungen vorgenommen."));
        }

        verticalLayout.addComponent(ok);
        setContent(verticalLayout);

        ok.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                close();
            }
        });
    }
}
