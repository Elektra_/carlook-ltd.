package org.carlookltd.gui.windows;


import com.vaadin.data.HasValue;
import com.vaadin.ui.*;
import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.process.control.AutoVerwaltenControl;
import org.carlookltd.ui.MyUI;

/**
 *
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class AutoUpdateWindow extends Window {

    public AutoUpdateWindow(AutoDTO autoDTO){
        AutoDTO copyDTO = autoDTO.copy();
        VerticalLayout layout = new VerticalLayout();
        center();
        Label h1 = new Label();
        h1.setCaption("Auto bearbeiten:");
        layout.addComponent(h1);
        layout.setMargin(true);


        final TextField marke = new TextField();
        marke.setCaption("Marke");
        marke.setValue(autoDTO.getMarke());
        final TextField beschreibung = new TextField();
        beschreibung.setCaption("Beschreibung");
        beschreibung.setValue(autoDTO.getBeschreibung());
        final TextField kennzeichen = new TextField();
        kennzeichen.setCaption("Kennzeichen");
        kennzeichen.setValue(autoDTO.getKennzeichen());
        final TextField baujahr = new TextField();
        baujahr.setCaption("Baujahr");
        baujahr.setValue(String.valueOf(autoDTO.getBaujahr()));

        final Button speichern = new Button();
        speichern.setCaption("Speichern");
        final Button abbrechen = new Button();
        abbrechen.setCaption("Abbrechen");

        marke.setSizeFull();
        baujahr.setSizeFull();
        kennzeichen.setSizeFull();
        beschreibung.setSizeFull();

        layout.addComponent(marke);
        layout.addComponent(baujahr);
        layout.addComponent(kennzeichen);
        layout.addComponent(beschreibung);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(speichern);
        horizontalLayout.addComponent(abbrechen);
        layout.addComponent(horizontalLayout);
        setContent(layout);

        baujahr.addValueChangeListener(new HasValue.ValueChangeListener<String>() {

            @Override
            public void valueChange(HasValue.ValueChangeEvent<String> valueChangeEvent) {
                try {
                    Integer.parseInt(baujahr.getValue());
                }
                catch (NumberFormatException e){
                    baujahr.clear();
                    Notification.show("Bitte geben Sie eine Ganzzahl ein (bspw. 1995)", Notification.Type.WARNING_MESSAGE);
                }
            }
        });

        speichern.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if(marke.getValue().equals("")){
                    Notification.show("Bitte geben Sie einen Wert für die Marke ein.");
                    marke.focus();
                    return;
                }
                else if(beschreibung.getValue().equals("")){
                    Notification.show("Bitte geben Sie einen Wert für die Beschreibung ein.");
                    beschreibung.focus();
                    return;
                }
                else if(kennzeichen.getValue().equals("")){
                    Notification.show("Bitte geben Sie einen Wert für das Kennzeichen ein.");
                    kennzeichen.focus();
                    return;
                }
                else if(baujahr.getValue().isEmpty()){
                    Notification.show("Bitte geben Sie einen Wert für das Baujahr ein.");
                    baujahr.focus();
                    return;
                }

                autoDTO.setMarke(marke.getValue());
                autoDTO.setBeschreibung(beschreibung.getValue());
                autoDTO.setKennzeichen(kennzeichen.getValue());
                autoDTO.setBaujahr(Integer.parseInt(baujahr.getValue()));
                autoDTO.setVertrieblerId(((MyUI) UI.getCurrent()).getUser().getBenutzerid());
                AutoVerwaltenControl.autoUpdate(autoDTO);
                AutoUpdateConfirmationWindow autoUpdateConfirmationWindow = new AutoUpdateConfirmationWindow(autoDTO,copyDTO);
                UI.getCurrent().addWindow(autoUpdateConfirmationWindow);

                autoUpdateConfirmationWindow.addCloseListener(new CloseListener() {
                    @Override
                    public void windowClose(CloseEvent closeEvent) {
                        close();
                    }
                });
            }

        });

        abbrechen.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                close();
            }
        });
    }
}
