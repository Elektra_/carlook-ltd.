package org.carlookltd.gui.windows;

import com.vaadin.ui.*;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class ConfirmationWindow extends Window {

    public ConfirmationWindow(String text) {
        super("Conformation"); //Set window caption
        center();

        //Some basic content for the window
        VerticalLayout content = new VerticalLayout();
        content.addComponent(new Label(text));
        content.setMargin(true);
        setContent(content);

        Button reservierungssButton = new Button("OK");
        reservierungssButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                close();
            }
        });
        content.addComponent(reservierungssButton);
        content.setComponentAlignment(reservierungssButton, Alignment.MIDDLE_CENTER);

    }
}
