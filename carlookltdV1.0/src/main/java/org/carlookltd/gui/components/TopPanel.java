package org.carlookltd.gui.components;

import com.vaadin.event.MouseEvents;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.Image;
import com.vaadin.ui.MenuBar;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.process.control.LoginControl;
import org.carlookltd.services.util.Rolle;
import org.carlookltd.services.util.Views;
import org.carlookltd.ui.MyUI;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class TopPanel extends HorizontalLayout {

    public TopPanel() {

        UserDTO user = ((MyUI) UI.getCurrent()).getUser();

        this.setSizeFull();
        this.setMargin(true);
        this.setWidth("100%");

        HorizontalLayout mainHorLayout = new HorizontalLayout();
        HorizontalLayout menuBarHorLayout = new HorizontalLayout();

        // Logo und Titel einbinden
        ThemeResource resource = new ThemeResource("img/carlooklogo.jpg");
        Image logo = new Image(null, resource);
        logo.setWidth("150px");
        if(user.getRolle().getBezeichnung().equals(Rolle.ENDKUNDE)) {
            logo.addClickListener(new MouseEvents.ClickListener() {
                @Override
                public void click(MouseEvents.ClickEvent clickEvent) {
                    UI.getCurrent().getNavigator().navigateTo(Views.MAIN);
                }
            });
        }

        //Label carlook_ltd = new Label("Carlook LTD", ContentMode.HTML);
        //carlook_ltd.addStyleName("carlookltd");

        mainHorLayout.addComponent(logo);
        //mainHorLayout.addComponent(carlook_ltd);
        mainHorLayout.setComponentAlignment(logo, Alignment.MIDDLE_LEFT);
        //mainHorLayout.setComponentAlignment(carlook_ltd, Alignment.MIDDLE_LEFT);
        this.addComponent(mainHorLayout);

        // Menubar erzeugen
        MenuBar menuBar = new MenuBar();
        MenuBar.MenuItem item1 = menuBar.addItem("Menu", null );

        //Logout des Users
        item1.addItem("Logout", FontAwesome.SIGN_OUT, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem menuItem) {
                // Navigation zu MainView
                LoginControl.logoutUser();
            }
        });

        //Reservierungen des Endkunden (und ggf. des Vertrieblers)
        if(user.getRolle().getBezeichnung().equals(Rolle.ENDKUNDE)) {
            item1.addItem("Reservierungen", FontAwesome.CAR, new MenuBar.Command() {
                @Override
                public void menuSelected(MenuBar.MenuItem menuItem) {
                    UI.getCurrent().getNavigator().navigateTo(Views.RESERVIERUNGEN);
                }
            });
        }

        /*
        if(user.getRolle().equals(Rolle.ENDKUNDE)) {
        item1.addItem("Cancel", FontAwesome.UNLINK, new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem menuItem) {
                //TODO: ein Window wird geöffnet, um Reservierungen anzuzeigen und ggf. zu stornieren
                ListReservierungWindow window = new ListReservierungWindow();
                UI.getCurrent().addWindow(window);

            }
        });
        }
         */

        //Name Eingeloggt als:
        Label nameEingeloggt = new Label("Eingeloggt als: " + user.getBenutzername());
        menuBarHorLayout.addComponent(nameEingeloggt);
        menuBarHorLayout.setComponentAlignment(nameEingeloggt, Alignment.MIDDLE_CENTER);

        menuBarHorLayout.addComponent(menuBar);
        this.addComponent(menuBarHorLayout);
        this.setComponentAlignment(menuBarHorLayout, Alignment.MIDDLE_RIGHT);
    }
}


