package org.carlookltd.ui;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import com.vaadin.navigator.Navigator;
import org.carlookltd.gui.views.*;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.services.util.Views;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */

@Theme("mytheme")
@Title("Carlook LTD")
@PreserveOnRefresh
public class MyUI extends UI {


    private UserDTO user = null;

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public UserDTO getUser() {
        return this.user;
    }



    @Override
    protected void init(VaadinRequest vaadinRequest) {

        Navigator navi = new Navigator(this, this);

        navi.addView(Views.MAIN, MainView.class);
        navi.addView(Views.LOGIN, LoginView.class);
        navi.addView(Views.REGISTRIERUNG, RegistrierungView.class);
        navi.addView(Views.RESERVIERUNGEN, ListReservierungView.class);
        navi.addView(Views.AUTOSVERWALTEN, AutosVerwaltenView.class);
        UI.getCurrent().getNavigator().navigateTo(Views.LOGIN);

    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = true)

    public static class MyUIServlet extends VaadinServlet {
    }
}
