package org.carlookltd.model.dao;

import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.carlookltd.model.dto.RoleDTO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.model.entity.User;
import org.carlookltd.process.control.exceptions.DatabaseException;
import org.carlookltd.process.control.exceptions.NoSuchUserOrPasswordException;
import org.carlookltd.process.control.exceptions.ViolatedUniqueConstraintException;
import org.carlookltd.services.db.DatabaseConnection;
import org.carlookltd.ui.MyUI;
import org.postgresql.util.PSQLException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class UserDAO extends AbstractDAO {

    public static UserDAO dao = null;

    private UserDAO() {

    }

    public static UserDAO getInstance() {
        if (dao == null) {
            dao = new UserDAO();
        }
        return dao;
    }

    public boolean checkIfBenutzernameExists(String benutzername) throws DatabaseException, ViolatedUniqueConstraintException {
        String sql = "SELECT benutzername "
                + "FROM autovermietung.benutzer WHERE upper(benutzername) = \'" + benutzername.toUpperCase() + "\'";
        PreparedStatement statement = this.getPreparedStatement(sql);

        ResultSet set;

        try {

            set = statement.executeQuery();
            if(set.next()){
                if(benutzername.toUpperCase().equals(set.getString(1).toUpperCase())) {
                    throw new ViolatedUniqueConstraintException("Dieser Benutzername ist bereits vergeben!");
                }
            }

        } catch (SQLException e) {
            throw new DatabaseException("Fehler beim Lesen aus der Datenbank!");
        }
        this.closeConnection();
        return false;
    }

    public boolean checkIfEmailExists(String email) throws DatabaseException, ViolatedUniqueConstraintException {
        //DatabaseConnection.getInstance().openConnection();
        String sql = "SELECT email "
                + "FROM autovermietung.benutzer WHERE upper(email) = \'" + email.toUpperCase() + "\'";
        PreparedStatement statement = this.getPreparedStatement(sql);

        ResultSet set;

        try {

            set = statement.executeQuery();
            if (set.next()){
                if (email.toUpperCase().equals(set.getString(1).toUpperCase())) {
                    throw new ViolatedUniqueConstraintException("Diese E-Mail adresse ist bereits vergeben!");
                }
            }
        } catch (SQLException e) {
            throw new DatabaseException("Fehler beim Lesen aus der Datenbank!");
        }
        this.closeConnection();
        return false;
    }

    public UserDTO getUserFromDB() throws DatabaseException{
        UserDTO res = new UserDTO();
        int userID = ((MyUI) UI.getCurrent()).getUser().getBenutzerid();
        String sql = "SELECT * FROM autovermietung.benutzer WHERE id = " + userID;
        PreparedStatement statement = this.getPreparedStatement(sql);
        ResultSet rs;

        try {
            rs = statement.executeQuery();

            if(rs == null){
                return null;
            }
            else if(rs.next()){
                res.setBenutzerid(rs.getInt(1));
                String rolle = rs.getString(2);
                RoleDTO roleDTO = new RoleDTO(rolle);
                res.setRolle(roleDTO);
                //res.setRolle(rs.getString(2));
                res.setBenutzername(rs.getString(3));
                res.setEmail(rs.getString(4));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DatabaseException("User konnte nicht geladen werden. Sichere Verbindung vorhanden?");
        }
        return res;
    }

    public int getMaxUserId() throws DatabaseException {
        int res = -99;
        String sql = "SELECT max(id) FROM autovermietung.benutzer";
        PreparedStatement preparedStatement = this.getPreparedStatement(sql);
        ResultSet rs;

        try {
            rs = preparedStatement.executeQuery();
            if(rs.next()){
                res = rs.getInt(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        this.closeConnection();
        return res;
    }

    public boolean createUser(User user, String rolle) throws DatabaseException {

        String sql = "INSERT INTO autovermietung.benutzer (benutzername,passwort,email) VALUES (?,?,?)";

        PreparedStatement statement = DatabaseConnection.getInstance().getPreparedStatement(sql);

        try {
            statement.setString(1, user.getBenutzername());
            statement.setString(2, user.getPasswort());
            statement.setString(3, user.getEmail());
            try {
                statement.execute();
            }
            catch (PSQLException e){
                // User bereits in DB vorhanden
                return false;
            }
            boolean createRole = RoleDAO.getInstance().createRole(user, rolle);
            System.out.println(rolle + " " + user.getBenutzername() + " erfolgreich registriert!");

            if(!createRole){
                throw new DatabaseException("Die Rolle für den Benutzer konnte nicht angelegt werden.");
            }
            user.setBenutzerid(UserDAO.getInstance().getMaxUserId());
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
            throw new DatabaseException("Beim Speichern der Daten ist ein Fehler aufgetreten!");
        }
        finally {
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        this.closeConnection();
        return true;
    }

    public UserDTO userLogin(String login, String password) throws DatabaseException, NoSuchUserOrPasswordException{
        //DB-Zugriff um den Benutzer beim Login aufzurufen - anhand JDBC (Framework, bietet Schnittstellen
        //zu relationalen Datenbanken verschiederner Hersteller)
        //JDBC iteriert über eine Liste mit Tupeln (Relationen werden in Listen dargestellt)

        ResultSet set;

        try {
            //DB-Zugriff
            String sql = "SELECT * "
                    + "FROM autovermietung.benutzer "
                    + "WHERE (upper(autovermietung.benutzer.benutzername) = \'" + login.toUpperCase() + "\'"
                    + " AND autovermietung.benutzer.passwort = \'" + password + "\')"
                    + " OR (upper(autovermietung.benutzer.email) = \'" + login.toUpperCase() + "\'"
                    + " AND autovermietung.benutzer.passwort = \'" + password + "\')";
            PreparedStatement statement = this.getPreparedStatement(sql);

            set = statement.executeQuery();


        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw new DatabaseException("Fehler im SQL-Befehl! Bitte den Programmierer benachrichtigen!");
        }


        UserDTO user = null;

        try {
            if (set.next()) {

                user = new UserDTO();
                user.setBenutzername(set.getString(2));
                user.setBenutzerid(set.getInt(1));
                user.setRolle(RoleDAO.getInstance().getRoleForUser(user));
                user.setEmail(set.getString(4));


            } else {
                //Error Handling
                throw new NoSuchUserOrPasswordException();

            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (DatabaseException e){
            Notification.show(e.getReason(),Notification.Type.ERROR_MESSAGE);
        }
        finally {
            this.closeConnection();
        }
        return user;
    }

}
