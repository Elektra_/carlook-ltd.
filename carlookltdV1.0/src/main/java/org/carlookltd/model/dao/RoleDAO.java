package org.carlookltd.model.dao;

import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.model.entity.User;
import org.carlookltd.process.control.LoginControl;
import org.carlookltd.model.dto.RoleDTO;
import org.carlookltd.process.control.exceptions.DatabaseException;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class RoleDAO extends AbstractDAO {

    //Verlagerung in diese Klasse, um die Verbindung mit der DB herzustellen
    public static RoleDAO dao = null;

    private RoleDAO() {

    }

    public static RoleDAO getInstance() {
        if (dao == null) {
            dao = new RoleDAO();
        }
        return dao;
    }

    public RoleDTO getRoleForUser(UserDTO user) throws DatabaseException {
        String sql = "SELECT * "
                + "FROM autovermietung.user_to_rolle "
                + "WHERE autovermietung.user_to_rolle.login = \'" + user.getBenutzername() + "\' ";
        PreparedStatement statement = this.getPreparedStatement(sql);

        ResultSet rs;

        try {
            rs = statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(LoginControl.class.getName()).log(Level.SEVERE, null, ex);
            throw new DatabaseException("Rolle konnte nicht geladen werden. Sichere Verbindung vorhanden?");
        }

        if(rs == null) {
            return null;
        }

        RoleDTO role = null;

        try {
            while (rs.next()) {
                role = new RoleDTO();
                role.setBezeichnung((rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginControl.class.getName()).log(Level.SEVERE, null, ex);
            throw new DatabaseException("Rolle konnte nicht geladen werden. Sichere Verbindung vorhanden?");
        }
        this.closeConnection();
        return role;
    }

    public boolean createRole(User user, String rolle) throws DatabaseException{
        String sql = "INSERT INTO autovermietung.user_to_rolle values (?,?)";
        PreparedStatement preparedStatement = this.getPreparedStatement(sql);
        try {
            preparedStatement.setString(1,user.getBenutzername());
            preparedStatement.setString(2,rolle);
            preparedStatement.execute();
            this.closeConnection();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DatabaseException("Rolle konnte nicht erstellt werden. Sichere Verbindung vorhanden?");
        }
    }

}
