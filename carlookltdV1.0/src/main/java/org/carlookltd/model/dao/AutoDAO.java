package org.carlookltd.model.dao;

import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.model.entity.Auto;
import org.carlookltd.process.control.exceptions.DatabaseException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class AutoDAO extends AbstractDAO{
    private static AutoDAO dao = null;

    private AutoDAO(){

    }

    public static AutoDAO getInstance(){
        if(dao == null){
            dao = new AutoDAO();
        }
        return dao;
    }

    public AutoDTO getAutoById(int id) throws DatabaseException{
        AutoDTO autoDTO = new AutoDTO();
        String sql = "SELECT * FROM autovermietung.auto WHERE id = " + id;
        PreparedStatement statement = this.getPreparedStatement(sql);
        try {
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                autoDTO.setAutoid(id);
                autoDTO.setMarke(rs.getString(2));
                autoDTO.setBaujahr(rs.getInt(3));
                autoDTO.setBeschreibung(rs.getString(4));
                autoDTO.setKennzeichen(rs.getString(5));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DatabaseException("Auto konnte nicht geladen werden. Sichere Verbindung vorhanden?");
        }
        this.closeConnection();
        return autoDTO;
    }


    public ArrayList<AutoDTO> getAutoByMarkeOrBezeichnungOrBaujahr(String suchbegriff) throws DatabaseException{
        ArrayList<AutoDTO> list = new ArrayList<>();
        AutoDTO auto;
        boolean isNumber;
        try {
            Integer.parseInt(suchbegriff);
            isNumber = true;
        }
        catch (NumberFormatException e){
            isNumber = false;
        }
        String sql;
        if(isNumber){
            sql = "SELECT * FROM autovermietung.auto WHERE upper(autovermietung.auto.marke) LIKE upper(\'" + suchbegriff + "%\')" +
                    "OR upper(autovermietung.auto.beschreibung) LIKE upper(\'%" + suchbegriff + "%\')" +
                    "OR cast(autovermietung.auto.baujahr as text) LIKE \'%" + suchbegriff + "%\'";
        }
        else {
            sql = "SELECT * FROM autovermietung.auto WHERE upper(autovermietung.auto.marke) LIKE upper(\'" + suchbegriff + "%\')" +
                    "OR upper(autovermietung.auto.beschreibung) LIKE upper(\'%" + suchbegriff + "%\')" ;
        }

        PreparedStatement statement = this.getPreparedStatement(sql);
        ResultSet rs;
        try {
            rs = statement.executeQuery();
        }
        catch (SQLException e){
            e.printStackTrace();
            throw new DatabaseException("Auto-Liste konnte nicht geladen werden. Sichere Verbindung vorhanden?");
        }

        if(rs == null){
            return null;
        }

        try {
            while (rs.next()){
                auto = new AutoDTO();
                auto.setAutoid(rs.getInt(1));
                auto.setMarke(rs.getString(2));
                auto.setBaujahr(rs.getInt(3));
                if(rs.getString(4) != null){
                    auto.setBeschreibung(rs.getString(4));
                }
                auto.setKennzeichen(rs.getString(5));
                list.add(auto);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
            throw new DatabaseException("Auto-Liste konnte nicht geladen werden. Sichere Verbindung vorhanden?");
        }
        this.closeConnection();
        return list;
    }

    public void createAuto(Auto auto) throws DatabaseException {
        String sql = "INSERT INTO autovermietung.auto" +
                " VALUES (default,?,?,?,?,?)";

        PreparedStatement statement = this.getPreparedStatement(sql);
        try {
            statement.setString(1,auto.getMarke());
            statement.setInt(2,auto.getBaujahr());
            statement.setString(3,auto.getBeschreibung());
            statement.setString(4,auto.getKennzeichen());
            statement.setInt(5,auto.getVertrieblerId());
            statement.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DatabaseException("Auto konnte nicht erstellt werden");
        }
    }

    public ArrayList<AutoDTO> getAutosByVertrieblerId(int vertrieblerId) throws DatabaseException {
        ArrayList<AutoDTO> list = new ArrayList<>();
        String sql = "select * from autovermietung.auto where vertriebler_id = " + vertrieblerId
                + "ORDER BY marke ASC";
        PreparedStatement statement = this.getPreparedStatement(sql);
        ResultSet rs;
        AutoDTO autoDTO;

        try {
            rs = statement.executeQuery();
            while(rs.next()){
                autoDTO = new AutoDTO();
                autoDTO.setAutoid(rs.getInt(1));
                autoDTO.setMarke(rs.getString(2));
                autoDTO.setBaujahr(rs.getInt(3));
                autoDTO.setBeschreibung(rs.getString(4));
                autoDTO.setKennzeichen(rs.getString(5));
                autoDTO.setVertrieblerId(rs.getInt(6));
                list.add(autoDTO);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DatabaseException("Auto konnte nicht geladen werden. Sichere Verbindung vorhanden?");
        }
        this.closeConnection();
        return list;
    }

    public void deleteAutoById(int autoid) throws DatabaseException{
        String sql = "delete from autovermietung.auto where id = " + autoid;
        PreparedStatement preparedStatement = this.getPreparedStatement(sql);

        try {
            preparedStatement.execute();
            this.closeConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DatabaseException("Auto konnte nicht gelöscht werden");
        }
    }

    public void updateAuto(Auto auto) throws DatabaseException{
        String sql = "update autovermietung.auto set " +
                "marke = \'" + auto.getMarke() + "\'," +
                "baujahr = " + auto.getBaujahr() + "," +
                "beschreibung = \'" + auto.getBeschreibung() + "\'," +
                "kennzeichen = \'" + auto.getKennzeichen() + "\' " +
                "where id = " + auto.getAutoid();

        PreparedStatement preparedStatement = this.getPreparedStatement(sql);
        try {
            preparedStatement.execute();
            this.closeConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DatabaseException("Auto konnte nicht überarbeitet werden");
        }

    }
}