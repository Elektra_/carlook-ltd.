package org.carlookltd.model.dao;

import org.carlookltd.model.dto.ReservierungDTO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.model.entity.Reservierung;
import org.carlookltd.process.control.exceptions.DatabaseException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class ReservierungDAO extends AbstractDAO {

    public static ReservierungDAO dao = null;

    private ReservierungDAO() {

    }
    public static ReservierungDAO getInstance() {
        if (dao == null) {
            dao = new ReservierungDAO();
        }
        return dao;
    }

    public void addReservierung(Reservierung reservierung) throws DatabaseException {
        String sql = "INSERT INTO autovermietung.reservierung (auto_id, benutzer_id, ausleih_datum, rueckgabe_datum, iban, buchungsdatum)" +
                " values (?,?,?,?,?,?);";
        PreparedStatement statement = this.getPreparedStatement(sql);

        //Zeilenweise Abbildung der Daten auf die Spalten der erzeugten Zeile
        try {
            statement.setDate(3, new java.sql.Date(reservierung.getAusleihDatum().getTime()));
            statement.setDate(4, new java.sql.Date(reservierung.getRueckgabeDatum().getTime()));
            statement.setString(5, reservierung.getIban());
            statement.setInt(1,reservierung.getAutoid());
            statement.setInt(2,reservierung.getEndkundeid());
            System.out.println(reservierung.getEndkundeid());
            statement.setDate(6,new java.sql.Date(reservierung.getBuchungsDatum().getTime()));

            statement.execute();

            //Nachträgliches Setzen der BuchungsID
            setReservierungsID(reservierung);

        } catch (SQLException ex) {
            Logger.getLogger(ReservierungDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new DatabaseException("Die Reservierung konnte nicht angelegt werden. Sichere Verbindung vorhanden?");
        }
        this.closeConnection();
    }

    private void setReservierungsID(Reservierung reservierung) throws DatabaseException{
        PreparedStatement statement = this.getPreparedStatement("SELECT max(autovermietung.reservierung.reservierungsnummer) "
                + "FROM autovermietung.reservierung ");

        ResultSet rs;

        try {
            rs = statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ReservierungDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new DatabaseException("Die ReservierungsID konnte nicht gefunden werden. Sichere Verbindung vorhanden?");
        }

        int currentValue;
        try {
            rs.next();
            currentValue = rs.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(ReservierungDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new DatabaseException("Die ReservierungsID konnte nicht gesetzt werden. Sichere Verbindung vorhanden?");
        }
        reservierung.setReservierungsnummer(currentValue);
        this.closeConnection();
    }

    public List<ReservierungDTO> getAllReservierungenForUser(UserDTO user) throws DatabaseException {
        String sql = "SELECT autovermietung.auto.id, autovermietung.auto.beschreibung, autovermietung.reservierung.reservierungsnummer, autovermietung.reservierung.ausleih_datum, autovermietung.reservierung.rueckgabe_datum, autovermietung.reservierung.buchungsdatum, autovermietung.reservierung.iban "
                + "FROM autovermietung.reservierung JOIN autovermietung.auto "
                + "ON ( autovermietung.reservierung.auto_id = autovermietung.auto.id )"
                + "WHERE autovermietung.reservierung.benutzer_id = " + user.getBenutzerid()
                + "ORDER BY reservierung.reservierungsnummer";
        PreparedStatement statement = this.getPreparedStatement(sql);

        ResultSet rs;

        try {
            rs = statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ReservierungDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new DatabaseException("Reservierungen konnten nicht geladen werden. Sichere Verbindung vorhanden?");
        }

        if(rs == null) return null;

        List<ReservierungDTO> liste = new ArrayList<>();
        ReservierungDTO reservierung;

        try {
            while (rs.next()) {
                reservierung = new ReservierungDTO();
                reservierung.setAutoId(rs.getInt(1));
                reservierung.setReservierungsnummer(rs.getInt(3));
                reservierung.setAusleihDatum(rs.getDate(4));
                reservierung.setRueckgabeDatum(rs.getDate(5));
                reservierung.setBuchungsDatum(rs.getDate(6));
                reservierung.setIban(rs.getString(7));
                reservierung.setAutoDTO(AutoDAO.getInstance().getAutoById(rs.getInt(1)));

                liste.add(reservierung);

            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservierungDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw new DatabaseException("Fehler bei der Erstellung des Objekts. Sichere Verbindung vorhanden?");
        }
        this.closeConnection();
        return liste;
    }

}
