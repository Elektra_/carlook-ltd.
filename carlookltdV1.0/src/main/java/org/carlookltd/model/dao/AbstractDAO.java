package org.carlookltd.model.dao;

import org.carlookltd.process.control.exceptions.DatabaseException;
import org.carlookltd.services.db.DatabaseConnection;
import java.sql.PreparedStatement;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class AbstractDAO {

    protected PreparedStatement getPreparedStatement(String sql) throws DatabaseException{
        PreparedStatement statement;
        statement = DatabaseConnection.getInstance().getPreparedStatement(sql);
        return statement;
    }

    protected void closeConnection() throws DatabaseException{
        DatabaseConnection.getInstance().closeConnection();
    }

}