package org.carlookltd.model.entity;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class Reservierung {

    private Date ausleihDatum = null;
    private Date rueckgabeDatum = null;
    private int endkundeid = -1;
    private int autoid = -1;
    private String iban;
    private int reservierungsnummer;
    private Date buchungsDatum = null;
    private User user;
    private Auto auto;

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Date getAusleihDatum() {
        return ausleihDatum;
    }

    public void setAusleihDatum(Date ausleihDatum) {
        this.ausleihDatum = ausleihDatum;
    }

    public Date getRueckgabeDatum() {
        return rueckgabeDatum;
    }

    public void setRueckgabeDatum(Date rueckgabeDatum) {
        this.rueckgabeDatum = rueckgabeDatum;
    }

    public int getEndkundeid() {
        return endkundeid;
    }

    public void setEndkundeid(int endkundeid) {
        this.endkundeid = endkundeid;
    }

    public int getAutoid() {
        return autoid;
    }

    public void setAutoid(int autoid) {
        this.autoid = autoid;
    }

    public int getReservierungsnummer() {
        return reservierungsnummer;
    }

    public void setReservierungsnummer(int reservierungsnummer) {
        this.reservierungsnummer = reservierungsnummer;
    }

    public Date getBuchungsDatum() {
        return buchungsDatum;
    }

    public void setBuchungsDatum(Date buchungsDatum) {
        this.buchungsDatum = buchungsDatum;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Auto getAuto() {
        return auto;
    }

    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservierung reservierung = (Reservierung) o;
        return autoid == reservierung.autoid &&
                endkundeid == reservierung.endkundeid &&
                Objects.equals(ausleihDatum, reservierung.ausleihDatum) &&
                Objects.equals(rueckgabeDatum, reservierung.rueckgabeDatum);
    }

}
