package org.carlookltd.model.entity;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class Auto {

    private String beschreibung = null;
    private String kennzeichen = null;
    private String marke = null;
    private int baujahr = 0;
    private int autoid = -1;
    private int vertrieblerId;

    public Auto() { }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public void setKennzeichen(String kennzeichen) {
        this.kennzeichen = kennzeichen;
    }

    public String getMarke() { return marke; }

    public void setMarke(String marke) { this.marke = marke; }

    public int getBaujahr() { return baujahr; }

    public void setBaujahr(int baujahr) { this.baujahr = baujahr; }

    public int getAutoid() { return autoid; }

    public void setAutoid(int autoid) { this.autoid = autoid; }

    public int getVertrieblerId() {
        return this.vertrieblerId;
    }

    public void setVertrieblerId(int vertrieblerId){
        this.vertrieblerId = vertrieblerId;
    }

    public String toString(){
        return this.beschreibung + " " + this.kennzeichen;
    }
}
