package org.carlookltd.model.entity;

import org.carlookltd.services.util.Rolle;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class User {

    private String benutzername = null;
    private String email = null;
    private String rolle = null;
    private String passwort = null;
    private int benutzerid = -1;

    public User() {}

    public User(String benutzername, String email, String rolle, String passwort, int benutzerid) {
        this.setBenutzername(benutzername);
        this.setEmail(email);
        this.setRolle(rolle);
        this.setPasswort(passwort);
        this.setBenutzerid(benutzerid);
    }

    public String getBenutzername() {
        return benutzername;
    }

    public void setBenutzername(String benutername) {
        this.benutzername = benutername;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getRolle() {
        return rolle;
    }

    public void setRolle(String rolle) {
        if (!rolle.equals(Rolle.ENDKUNDE) && !rolle.equals(Rolle.VERTRIEBLER) && !rolle.equals(Rolle.ADMIN)) {
            throw new IllegalArgumentException("Ungültige Benutzerrolle");
        }
        this.rolle = rolle;
    }

    public int getBenutzerid() {
        return benutzerid;
    }

    public void setBenutzerid(int benutzerid) {
        this.benutzerid = benutzerid;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String toString(){
        return this.benutzername + "  | " + this.email;
    }
}
