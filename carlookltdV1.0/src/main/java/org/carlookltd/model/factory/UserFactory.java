package org.carlookltd.model.factory;

import org.carlookltd.model.entity.User;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class UserFactory {

    public static User benutzerMitBenutzernamePasswortEmailRolle(String benutzername, String passwort, String email, String rolle) {
    User user = new User();
    user.setBenutzername(benutzername);
    user.setPasswort(passwort);
    user.setEmail(email);
    user.setRolle(rolle);
    return user;
    }
}
