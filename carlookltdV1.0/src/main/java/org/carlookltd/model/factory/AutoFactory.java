package org.carlookltd.model.factory;

import org.carlookltd.model.entity.Auto;

/**
 *
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class AutoFactory {

    public static Auto createAuto(org.carlookltd.model.dto.AutoDTO autoDTO){
        Auto auto = new Auto();
        auto.setAutoid(autoDTO.getAutoid());
        auto.setMarke(autoDTO.getMarke());
        auto.setKennzeichen(autoDTO.getKennzeichen());
        auto.setBaujahr(autoDTO.getBaujahr());
        auto.setBeschreibung(autoDTO.getBeschreibung());
        auto.setVertrieblerId(autoDTO.getVertrieblerId());
        return auto;
    }
}
