package org.carlookltd.model.factory;

import org.carlookltd.model.dto.ReservierungDTO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.model.entity.Reservierung;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class ReservierungFactory {
    public static Reservierung createBooking(ReservierungDTO reservierungDTO, UserDTO user) {

        Reservierung reservierung = new Reservierung();

        reservierung.setAusleihDatum(reservierungDTO.getAusleihDatum());
        reservierung.setRueckgabeDatum(reservierungDTO.getRueckgabeDatum());
        reservierung.setIban(reservierungDTO.getIban());
        reservierung.setBuchungsDatum(reservierungDTO.getBuchungsDatum());
        reservierung.setAutoid(reservierungDTO.getAutoDTO().getAutoid());

        reservierung.setEndkundeid(user.getBenutzerid());

        //Reservierungsnummer wird später bei der Ablage in die Datenbank hinzugefügt

        return reservierung;

    }

}
