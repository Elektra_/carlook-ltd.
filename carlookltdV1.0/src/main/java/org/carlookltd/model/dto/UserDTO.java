package org.carlookltd.model.dto;

import com.vaadin.ui.Notification;
import org.carlookltd.model.dao.RoleDAO;
import org.carlookltd.process.control.exceptions.DatabaseException;

import java.io.Serializable;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class UserDTO implements Serializable {

    private String benutzername = null;
    private String email = null;
    private RoleDTO rolle = null;
    private String passwort = null;
    private int benutzerid = -1;

    public UserDTO() {}

    public String getBenutzername() {
        return benutzername;
    }

    public void setBenutzername(String benutername) {
        this.benutzername = benutername;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public RoleDTO getRolle() {
        try {
            return RoleDAO.getInstance().getRoleForUser(this);
        } catch (DatabaseException e) {
            e.printStackTrace();
            Notification.show(e.getReason(),Notification.Type.ERROR_MESSAGE);
        }
        return null;
    }

    public void setRolle(RoleDTO rolle) {
        this.rolle = rolle;
    }

    public int getBenutzerid() {
        return benutzerid;
    }

    public void setBenutzerid(int benutzerid) {
        this.benutzerid = benutzerid;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

}
