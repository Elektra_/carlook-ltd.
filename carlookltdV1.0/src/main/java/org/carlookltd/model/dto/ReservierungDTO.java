package org.carlookltd.model.dto;

import java.util.Date;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class ReservierungDTO {

    private Date ausleihDatum = null;
    private Date rueckgabeDatum = null;
    private String iban;
    private int reservierungsnummer;
    private Date buchungsDatum = null;
    private UserDTO user;
    private AutoDTO autoDTO;
    private int autoId;


    public ReservierungDTO(){}

   /* public ReserierungDTO(EndkundeDTO endkunde, AutoDTO auto) {

        this.endkunde = endkunde;
        this.auto = auto;
    } */

    public int getReservierungsnummer() {
        return reservierungsnummer;
    }

    public void setReservierungsnummer(int reservierungsnummer) {
        this.reservierungsnummer = reservierungsnummer;
    }

    public int getAutoId() {
        return autoId;
    }

    public void setAutoId(int autoId) {
        this.autoId = autoId;
    }

    public Date getBuchungsDatum() {
        return buchungsDatum;
    }

    public void setBuchungsDatum(Date buchungsDatum) {
        this.buchungsDatum = buchungsDatum;
    }

    public Date getAusleihDatum() {
        return ausleihDatum;
    }

    public void setAusleihDatum(Date ausleihDatum) {
        this.ausleihDatum = ausleihDatum;
    }

    public Date getRueckgabeDatum() {
        return rueckgabeDatum;
    }

    public void setRueckgabeDatum(Date rueckgabeDatum) {
        this.rueckgabeDatum = rueckgabeDatum;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public AutoDTO getAutoDTO() {
        return autoDTO;
    }

    public void setAutoDTO(AutoDTO autodto) {
        this.autoDTO = autodto;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

}

