package org.carlookltd.model.dto;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class RoleDTO {
    private String bezeichnung = null;

    public RoleDTO() {}

    public RoleDTO (String rolle) {
        this.bezeichnung = rolle;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

}
