package org.carlookltd.model.dto;

import java.io.Serializable;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class AutoDTO implements Serializable {

    private String beschreibung = null;
    private String kennzeichen = null;
    private String marke = null;
    private int baujahr = 0;
    private int autoid = -1;

    //private Vertriebler vertriebler = null;
    //private int vertrieblerid = -1;

    public AutoDTO() { }

    public AutoDTO(String beschreibung, String kennzeichen, String marke, int baujahr) {
        this.beschreibung = beschreibung;
        this.kennzeichen = kennzeichen;
        this.marke = marke;
        this.baujahr = baujahr;
    }

    /*
    public AutoDTO(Auto auto) {
        this.setMarke(auto.getMarke());
        this.setBeschreibung(auto.getBeschreibung());
        this.setBaujahr(auto.getBaujahr());
        this.setKennzeichen(auto.getKennzeichen());
        this.setAutoid(auto.getAutoid());
        this.setVertrieblerId(auto.getVertrieblerId());
    }

     */

    public int getVertrieblerId() {
        return vertrieblerId;
    }

    public void setVertrieblerId(int vertrieblerId) {
        this.vertrieblerId = vertrieblerId;
    }

    private int vertrieblerId;

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public void setKennzeichen(String kennzeichen) {
        this.kennzeichen = kennzeichen;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public int getAutoid() {
        return autoid;
    }

    public void setAutoid(int autoid) {
        this.autoid = autoid;
    }

    public String toString(){
        return this.marke + " " + this.beschreibung + " " + this.kennzeichen;
    }

    public AutoDTO copy(){
        AutoDTO newAutoDTO = new AutoDTO();
        newAutoDTO.setAutoid(this.autoid);
        newAutoDTO.setVertrieblerId(this.vertrieblerId);
        newAutoDTO.setBaujahr(this.baujahr);
        newAutoDTO.setBeschreibung(this.beschreibung);
        newAutoDTO.setMarke(this.marke);
        newAutoDTO.setKennzeichen(this.kennzeichen);
        return newAutoDTO;
    }
}
