package org.carlookltd.services.db;

import org.carlookltd.process.control.exceptions.DatabaseException;

import java.sql.*;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class DatabaseConnection {
    // Lokale Datenbankverbindung
    private Connection connection = null;
    // Connectionstring zur PostgreSQL Datenbank
    private static final String url = "jdbc:postgresql://dumbo.inf.h-brs.de:5432/bceti12s";
    // Benutzer der Datenbank
    private static final String user = "bceti12s";
    // Passwort des Benutzers
    private static final String password =  DBpsw.PASSWORT;
    private static DatabaseConnection dbcon = null;

    public DatabaseConnection() throws DatabaseException {
        openConnection();
    }

    public void openConnection() throws DatabaseException {
        try {
            if(this.connection == null || this.connection.isClosed()) {
                this.connection = DriverManager.getConnection(url, user, password);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
            throw new DatabaseException("Fehler bei Zugriff auf die DB: Sichere Verbindung vorhanden?!");
        }

    }

    public static DatabaseConnection getInstance() throws DatabaseException {
        if(dbcon == null){
            dbcon = new DatabaseConnection();
        }
        return dbcon;
    }

    public PreparedStatement getPreparedStatement(String sql) throws DatabaseException {
        PreparedStatement s;
        try {
            if (this.connection.isClosed()) {
                this.openConnection();
            }
            s = this.connection.prepareStatement(sql);
        } catch (SQLException throwables){
            throwables.printStackTrace();
            throw new DatabaseException("PreparedStatement konnte nicht erstellt werden. Sichere Verbindung vorhanden?");
        }
        return s;
    }

    /**
     * Schließt die SQL-Connection
     */
    public void closeConnection() throws DatabaseException {
        try {
            if(!this.connection.isClosed()){
                this.connection.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DatabaseException("Die Verbindung konnte nicht geschlossen werden");
        }
    }

}
