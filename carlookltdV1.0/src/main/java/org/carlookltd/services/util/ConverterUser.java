package org.carlookltd.services.util;

import org.carlookltd.model.dto.RoleDTO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.model.entity.User;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class ConverterUser {
    public static UserDTO convertToDTO(User user){
        UserDTO result = new UserDTO();
        result.setBenutzername(user.getBenutzername());
        result.setEmail(user.getEmail());
        result.setPasswort(user.getPasswort());
        result.setBenutzerid(user.getBenutzerid());
        String rolle = user.getRolle();
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setBezeichnung(rolle);
        result.setRolle(roleDTO);
        return result;
    }
}
