package org.carlookltd.services.util;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class TextFieldUtil {
    private TextFieldUtil(){}
    public static final String BENUTZERNAME ="Benutzername";
    public static final String PASSWORT = "Passwort";
    public static final String PASSWORT_CONFIRM = "Passwort bestätigen";
    public static final String LOGIN = "Login";
    public static final String REGISTRIEREN = "Registrieren";
    public static final String ABBRECHEN = "Abbrechen";
    public static final String RESERVIEREN = "Reservieren";
    public static final String SUCHEN = "Suchen";
    public static final String EMAIL = "E-Mail";
    public static final String EMAIL_CONFIRM = "E-Mail bestätigen";
    public static final String COMBOBOX_ROLLE = "cmbRolle";
    public static final String ROLLE_ENDKUNDE = "rolleEndkunde";
    public static final String ROLLE_VERTRIEBLER = "rolleVertriebler";
}
