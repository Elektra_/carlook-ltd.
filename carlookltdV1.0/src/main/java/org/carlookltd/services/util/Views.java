package org.carlookltd.services.util;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class Views {

    public static final String MAIN = "Startseite";
    public static final String LOGIN = "Login";
    public static final String REGISTRIERUNG = "Registrierung";
    public static final String RESERVIERUNGEN = "Reservierungen";
    public static final String AUTOSVERWALTEN = "Autos verwalten";

}
