package org.carlookltd.services.util;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class Rolle {
    public final static String ENDKUNDE = "endkunde";
    public final static String VERTRIEBLER = "vertriebler";
    public final static String ADMIN = "admin";

}
