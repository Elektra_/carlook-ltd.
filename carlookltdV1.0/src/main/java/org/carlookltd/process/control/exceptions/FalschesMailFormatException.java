package org.carlookltd.process.control.exceptions;
public class FalschesMailFormatException extends RegistrierungException{
    public FalschesMailFormatException(String reason) {
        super(reason);
    }
}
