package org.carlookltd.process.control.exceptions;

public class ViolatedUniqueConstraintException extends RegistrierungException {
    private String reason = null;
    public ViolatedUniqueConstraintException(String reason) {
        super(reason);
        this.reason = reason;
    }
    public String getReason() {
        return this.reason;
    }
}
