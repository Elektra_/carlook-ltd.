package org.carlookltd.process.control;

import org.carlookltd.model.dao.AutoDAO;
import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.model.factory.AutoFactory;
import org.carlookltd.process.control.exceptions.DatabaseException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class AutoVerwaltenControl {

    private static final Logger LOGGER = Logger.getLogger( AutoVerwaltenControl.class.getName() );

    public static void autoErstellen(AutoDTO autoDTO){
        try {
            AutoDAO.getInstance().createAuto(AutoFactory.createAuto(autoDTO));
        } catch (DatabaseException e) {
            LOGGER.log( Level.SEVERE, e.toString(), e );
        }
    }

    public static void autoUpdate(AutoDTO autoDTO){
        try {
            AutoDAO.getInstance().updateAuto(AutoFactory.createAuto(autoDTO));
        } catch (DatabaseException e) {
            LOGGER.log( Level.SEVERE, e.toString(), e );
        }
    }

    public static void autoLoeschen(AutoDTO autoDTO){
        try {
            AutoDAO.getInstance().deleteAutoById(autoDTO.getAutoid());
        } catch (DatabaseException e) {
            LOGGER.log( Level.SEVERE, e.toString(), e );
        }
    }
}
