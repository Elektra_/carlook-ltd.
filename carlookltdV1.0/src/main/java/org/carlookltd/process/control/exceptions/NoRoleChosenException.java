package org.carlookltd.process.control.exceptions;
public class NoRoleChosenException extends RegistrierungException{
    public NoRoleChosenException(String reason)
    {
        super(reason);
    }
}
