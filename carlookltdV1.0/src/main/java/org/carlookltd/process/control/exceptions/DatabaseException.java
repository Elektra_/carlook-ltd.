package org.carlookltd.process.control.exceptions;

public class DatabaseException extends Exception {
    private String reason = null;

    public DatabaseException(String reason) {
        super(reason);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

}
