package org.carlookltd.process.control.exceptions;

public class UngleicheMailsException extends RegistrierungException{

    public UngleicheMailsException(String reason) {
        super(reason);
    }
}
