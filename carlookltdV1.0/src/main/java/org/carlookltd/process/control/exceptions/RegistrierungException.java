package org.carlookltd.process.control.exceptions;
public abstract class RegistrierungException extends Exception{
    public RegistrierungException(String reason) {
        super(reason);
    }
}
