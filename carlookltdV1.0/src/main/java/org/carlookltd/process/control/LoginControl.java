package org.carlookltd.process.control;

import com.vaadin.ui.*;
import org.carlookltd.model.dao.UserDAO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.process.control.exceptions.*;
import org.carlookltd.services.util.Rolle;
import org.carlookltd.services.util.Views;
import org.carlookltd.ui.MyUI;

/**
 *
 * @author Julia Lepp
 * @author Bugay Cetinel
 * @version 1.0
 *
 */

public class LoginControl {

    public static void checkAuthentication(String login, String password) throws DatabaseException, NoSuchUserOrPasswordException {

        //globale Variable in der man die UI-Daten speichert (Key-Value-Verfahren)
        //UI wird personalisiert für einen bestimmten Benutzer

        //VaadinSession session = UI.getCurrent().getSession();
        //session.setAttribute(Roles.CURRENT_USER, user);

        ((MyUI) UI.getCurrent()).setUser(UserDAO.getInstance().userLogin(login,password));

        UserDTO user = ((MyUI) UI.getCurrent()).getUser();

        if (user.getRolle().getBezeichnung().equals(Rolle.ENDKUNDE)) {
            //Der Benutzer ist nun vorhanden, nächster Schritt:
            UI.getCurrent().getNavigator().navigateTo(Views.MAIN);
        }
        else {
            UI.getCurrent().getNavigator().navigateTo(Views.AUTOSVERWALTEN);
        }

    }

    public static void logoutUser() {
        UI.getCurrent().close();

        UI.getCurrent().getPage().setLocation("/carlookltdV1.0");
        //UI.getCurrent().getSession().close();

    }
}
