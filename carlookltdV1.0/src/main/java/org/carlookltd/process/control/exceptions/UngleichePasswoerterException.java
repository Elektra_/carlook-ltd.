package org.carlookltd.process.control.exceptions;

public class UngleichePasswoerterException extends RegistrierungException{

    public UngleichePasswoerterException(String reason) {
        super(reason);
    }
}
