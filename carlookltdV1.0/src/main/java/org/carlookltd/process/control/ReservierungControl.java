package org.carlookltd.process.control;

import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.carlookltd.model.dao.ReservierungDAO;
import org.carlookltd.model.dto.ReservierungDTO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.model.entity.Reservierung;
import org.carlookltd.model.factory.ReservierungFactory;
import org.carlookltd.process.control.exceptions.DatabaseException;
import org.carlookltd.ui.MyUI;
import java.util.List;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class ReservierungControl {

    public static ReservierungControl process = null;

    public static ReservierungControl getInstance() {
        if (process == null) {
            process = new ReservierungControl();
        }
        return process;
    }

    public boolean checkReserverierung(ReservierungDTO request) {

        UserDTO user = ((MyUI) UI.getCurrent()).getUser();

        Reservierung reservierung = ReservierungFactory.createBooking(request, user);

        try {
            ReservierungDAO.getInstance().addReservierung(reservierung);
            request.setReservierungsnummer(reservierung.getReservierungsnummer());
        } catch (DatabaseException e) {
            e.printStackTrace();
            Notification.show("Fehler bei der Erstellung. Sichere Verbindung vorhanden? ", Notification.Type.ERROR_MESSAGE);
        }

        return true;
    }

    public List<ReservierungDTO> getAllReservierungForUser() {
        final UserDTO user = ((MyUI) UI.getCurrent()).getUser();
        List<ReservierungDTO> list = null;
        try {
            list = ReservierungDAO.getInstance().getAllReservierungenForUser(user);
        } catch (DatabaseException e) {
            e.printStackTrace();
            Notification.show(e.getReason(),Notification.Type.ERROR_MESSAGE);
        }
        return list;
    }
}
