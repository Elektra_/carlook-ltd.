package org.carlookltd.process.control.exceptions;
public class EmptyFieldsException extends RegistrierungException{

    public EmptyFieldsException(String reason) {
        super(reason);
    }

}
