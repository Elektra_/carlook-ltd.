package org.carlookltd.process.control;

import com.vaadin.ui.Notification;
import org.carlookltd.model.dao.AutoDAO;
import org.carlookltd.model.dto.AutoDTO;
import org.carlookltd.process.control.exceptions.DatabaseException;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bugay Cetinel
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class SucheControl {

    private SucheControl() {

    }
    public static SucheControl search = null;

    public static SucheControl getInstance() {
        if (search == null) {
            search = new SucheControl();
        }
        return search;
    }

    public List<AutoDTO> getAutoByMarkeOrBezeichnungOrBaujahr(String suchbegriff){
        ArrayList<AutoDTO> liste = new ArrayList<>();
        AutoDAO dao = AutoDAO.getInstance();
        try {
            liste = dao.getAutoByMarkeOrBezeichnungOrBaujahr(suchbegriff);
        } catch (DatabaseException e) {
            e.printStackTrace();
            Notification.show(e.getReason(),Notification.Type.ERROR_MESSAGE);
        }
        return liste;
    }
}
