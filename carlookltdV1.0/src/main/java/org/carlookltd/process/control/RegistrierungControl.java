package org.carlookltd.process.control;

import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.carlookltd.services.util.ConverterUser;
import org.carlookltd.model.dao.UserDAO;
import org.carlookltd.model.dto.UserDTO;
import org.carlookltd.model.entity.User;
import org.carlookltd.model.factory.UserFactory;
import org.carlookltd.process.control.exceptions.*;
import org.carlookltd.services.util.Rolle;
import org.carlookltd.services.util.Views;
import org.carlookltd.ui.MyUI;

/**
 *
 * @author Julia Lepp
 * @version 1.0
 *
 */

public class RegistrierungControl {

    private static RegistrierungControl instance;

    private RegistrierungControl() {
    }

    public static RegistrierungControl getInstance() {
        if (RegistrierungControl.instance == null) {
            RegistrierungControl.instance = new RegistrierungControl();
        }
        return RegistrierungControl.instance;
    }

    public void checkRegistrierung(String login, String password, String passwortrepeat,
                                   String email, String emailrepeat, String rolle) throws RegistrierungException, DatabaseException {

        User user = null;

            if (login == null || login.equals("") || password == null || password.equals("")
                    || passwortrepeat == null || passwortrepeat.equals("") || email == null ||
                    email.equals("") || emailrepeat == null || emailrepeat.equals("")) {
                throw new EmptyFieldsException("Sie müssen alle Felder ausfüllen.");
            }
            if (!password.equals(passwortrepeat)) {
                throw new UngleichePasswoerterException("Die Passwörter müssen übereinstimmen.");
            }
            //Fehler-Handling E-Mail ausbaufähig
            if (rolle.equals(Rolle.VERTRIEBLER) && !email.contains("@carlook.de") || rolle.equals(Rolle.VERTRIEBLER) && email.startsWith("@")) {
                throw new FalschesMailFormatException("So sollte Ihre E-Mailadresse aussehen: name@carlook.de");
            }
            if (rolle.equals(Rolle.ENDKUNDE) && email.startsWith("@") || rolle.equals(Rolle.ENDKUNDE) && !email.contains("@")) {
                throw new FalschesMailFormatException("Bitte überprüfen Sie die E-Mailadresse.");
            }
            if (!email.equals(emailrepeat)) {
                throw new UngleicheMailsException("Die E-Mailadressen müssen übereinstimmen.");
            }
            if (rolle.isEmpty()) {
                throw new NoRoleChosenException("Sie müssen auswählen, ob Sie Endkunde oder Vertriebler sind.");
            }
            if(UserDAO.getInstance().checkIfBenutzernameExists(login) || UserDAO.getInstance().checkIfEmailExists(email)) {
                throw new ViolatedUniqueConstraintException("Der Benutzer ist bereits vorhanden.");
            }


            user = UserFactory.benutzerMitBenutzernamePasswortEmailRolle(login, password, email, rolle);
            boolean registerOk;

            try {

                if (rolle.equals(Rolle.ENDKUNDE)) {
                    registerOk = UserDAO.getInstance().createUser(user, Rolle.ENDKUNDE);
                } else if (rolle.equals(Rolle.VERTRIEBLER)) {
                    registerOk = UserDAO.getInstance().createUser(user, Rolle.VERTRIEBLER);
                }
                else {
                    registerOk = false;
                }

                if(!registerOk){
                    Notification.show("Der Benutzername ist bereits vorhanden.", Notification.Type.WARNING_MESSAGE);
                    return;
                }
            }
            catch (DatabaseException e) {
                Notification.show("DB-Fehler", e.getMessage() , Notification.Type.ERROR_MESSAGE);
                e.printStackTrace();
            }

        UserDTO userDTO = ConverterUser.convertToDTO(user);

        //globale Variable in der man die UI-Daten speichert (Key-Value-Verfahren)
        //UI wird personalisiert für einen bestimmten Benutzer
        ((MyUI) UI.getCurrent()).setUser(userDTO);

        if (userDTO.getRolle().getBezeichnung().equals(Rolle.ENDKUNDE)) {
            //Der Benutzer ist nun vorhanden, nächster Schritt:
            UI.getCurrent().getNavigator().navigateTo(Views.MAIN);
        }
        else {
            UI.getCurrent().getNavigator().navigateTo(Views.AUTOSVERWALTEN);
        }

    }
}
